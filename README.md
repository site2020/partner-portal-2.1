# Onyx (Front-End) #

Onyx is a stand-alone product for the Traffic Control Industry, created and deisgned by Site 2020. The sole purpose of this product
is to provide the Traffic Control Industry with a standardized dispatch, employee payroll and client invoicing system that 
is created for their industry specifically. 

### What is Onyx built with? ###
* Front-End - VueJS/HTML
* API - Flask/SQL Alchemy (separate repo)

### Planned Changes and Updates ###
Currently, our Fleet API is being run/maintained in a PHP based environment. The plan is to eventually move
this API out of PHP and have it run in a Flask/SQLAlchemy environment instead. Once this happens, we will also have to move 
key API calls from within Onyx. 
* JSONHandler moved to FleetAPI (Flask)
* OnyxAPI needs to have some of the SQL's rewritten to better take advantage of SQLAlchemy and Flask.
* There are some small quirks on the Front-End that could use some cleaning up. 
* The project is still in development and will inevitably require work.
* Better integration with Sage, QuickBooks etc.

### How do I get set up? ###

* Project Setup - `yarn install`
* Compiles and Hot-Reloads for Development - `yarn run serve`
* Compile and Minifies for Production - `yarn run build`
* Lints and Fixes Files `yarn run lint`

### Who do I talk to? ###

If you have any questions regarding the project please reach out to one of the following people;
* Anthony Probert (anthony@site2020.com)
* Mike Matthews (mmatthews@site2020.com)