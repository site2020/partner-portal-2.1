import DashboardLayout from "@/pages/Dashboard/Layout/DashboardLayout.vue";
import AuthLayout from "@/pages/Dashboard/Pages/AuthLayout.vue";

// Dashboard pages
import Dashboard from "@/pages/Dashboard/Dashboard.vue";

//Employee Management
import AddUser from "@/pages/Dashboard/Pages/EmployeeManagement/Employee-Accounts/Add-User/AddUser.vue";
import ManageUser from "@/pages/Dashboard/Pages/EmployeeManagement/Employee-Accounts/Manage-User/ManageUser.vue";

//Stay in Touch
import News from "@/pages/Dashboard/Pages/StayInTouch/News/News.vue";
import Feedback from "@/pages/Dashboard/Pages/StayInTouch/Feedback/Feedback.vue";

//Marketing
import Tracker from "@/pages/Dashboard/Marketing/Trackers/Trackers.vue";
import Onyx from "@/pages/Dashboard/Marketing/Onyx/Onyx.vue";

//Site2020Reports
import CustomerSuccessReport from "@/pages/Dashboard/Pages/Site2020Reporting/CustomerSuccessReport/CustomerSuccessReport";
import DepSalesManagement from "@/pages/Dashboard/Pages/Site2020Reporting/DepSalesManagement/DepSalesManagement";
import FleetUsageReport from "@/pages/Dashboard/Pages/Site2020Reporting/FleetUsageReport/FleetUsageReport";
import ManageNag from "@/pages/Dashboard/Pages/Site2020Reporting/ManageNag/ManageNag";

// Pages
import ViewSessions from "@/pages/Dashboard/Pages/Sessions/ViewSessions/ViewSessions.vue";
import ViewInvoices from "@/pages/Dashboard/Pages/Sessions/ViewInvoices/ViewInvoices";
import User from "@/pages/Dashboard/Pages/UserProfile/UserProfile.vue";
import NewsPosts from "@/pages/Dashboard/Pages/NewsPosts/NewsPosts.vue";
import Login from "@/pages/Dashboard/Pages/Login.vue";
import BasestationPassthrough from "@/pages/Dashboard/Pages/BasestationPassthrough/BasestationPassthrough.vue";
import SubOffices from "@/pages/Dashboard/Pages/SubOfficeManagement/SubOffices.vue";

let pagesMenu = {
  path: "/pages",
  component: DashboardLayout,
  name: "Pages",
  redirect: "/pages/user",
  children: [
    {
      path: "user",
      name: "User Page",
      components: { default: User },
    },
    {
      path: "news-posts",
      name: "News Posts",
      components: { default: NewsPosts },
      meta: { requireAdmin: true },
    },
    {
      path: "add-user",
      name: "Add User",
      components: { default: AddUser },
    },
    {
      path: "manage-user",
      name: "Manage User",
      components: { default: ManageUser },
    },
    {
      path: "view-sessions",
      name: "View Sessions",
      components: { default: ViewSessions },
    },
    {
      path: "view-invoices",
      name: "View Invoices",
      components: { default: ViewInvoices },
    },
    {
      path: "basestation-passthrough",
      name: "Basestation Passthrough",
      components: { default: BasestationPassthrough },
      meta: { requireAuth: true },
    },
    {
      path: "news",
      name: "News",
      components: { default: News },
    },
    {
      path: "feedback",
      name: "Feedback",
      components: { default: Feedback },
    },
    {
      path: "/dashboard/:SelectedSession",
      name: "SelectedSession",
      components: { default: Dashboard },
    },
    {
      path: "/tracker",
      name: "Trackers",
      components: { default: Tracker },
    },
    {
      path: "/onyx",
      name: "Onyx",
      components: { default: Onyx },
    },
    {
      path: "customer-success-report",
      name: "Customer Success Report",
      components: { default: CustomerSuccessReport },
    },
    {
      path: "dep-sales-management",
      name: "Dep Sales Management",
      components: { default: DepSalesManagement },
    },
    {
      path: "fleet-usage-report",
      name: "Fleet Usage Report",
      components: { default: FleetUsageReport },
    },
    {
      path: "manage-nags",
      name: "Manage NAGs",
      components: { default: ManageNag },
    },
    {
      path: "sub-office-management",
      name: "Manage Sub Offices",
      components: { default: SubOffices },
    },
  ],
};

let authPages = {
  path: "/",
  component: AuthLayout,
  name: "Authentication",
  children: [
    {
      path: "/login",
      name: "Login",
      component: Login,
    },
  ],
};

const routes = [
  {
    path: "/",
    redirect: "/dashboard",
    name: "Home",
  },
  pagesMenu,
  authPages,
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        components: { default: Dashboard },
      },
    ],
  },
];

export default routes;
