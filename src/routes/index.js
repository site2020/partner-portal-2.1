import VueRouter from "vue-router";
import store from "@/state/store";
import routes from "./routes";
import Vue from "vue";
import Swal from "sweetalert2";

Vue.use(VueRouter);

const router = new VueRouter({
  routes, // short for routes: routes
  scrollBehavior: (to) => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  },
  linkExactActiveClass: "nav-item active",
});

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {
    if (localStorage.getItem("S2020_USERTYPE") == "PARTNERADMIN") {
      next();
    } else {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: "md-button md-danger",
        },
        buttonsStyling: false,
      });
      swalWithBootstrapButtons.fire({
        title: "Permission Error!",
        text: "You do not have permission to access this page.",
        icon: "error",
        confirmButtonText: "Continue",
      });
      next({ name: "Dashboard" });
    }
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.meta.requireAdmin) {
    if (localStorage.getItem("S2020_USERTYPE") == "PARTNERADMIN") {
      next();
    } else {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: "md-button md-danger",
        },
        buttonsStyling: false,
      });
      swalWithBootstrapButtons.fire({
        title: "Permission Error!",
        text: "You do not have permission to access this page.",
        icon: "error",
        confirmButtonText: "Continue",
      });
      next({ name: "Dashboard" });
    }
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.name !== "Login" && localStorage.getItem("S2020_AUTHKEY") == null)
    next({ name: "Login" });
  else next();
});

export default router;
