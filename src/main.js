import Vue from "vue";
import VueRouter from "vue-router";
import DashboardPlugin from "./material-dashboard";
import store from "./state/store";

//APIs
import api from "./services/api";
import fleetapi from "./services/fleetapi";
import onyxapi from "./services/onyxapi";

// Plugins
import { FormRadioPlugin } from "bootstrap-vue";
import VueTimeago from "vue-timeago";
Vue.use(FormRadioPlugin);
import App from "./App.vue";
import Chartist from "chartist";
import moment from "moment-timezone";
Vue.prototype.moment = moment;
import VueExcelXlsx from "vue-excel-xlsx";
import { TablePlugin } from "bootstrap-vue";
Vue.use(TablePlugin);

// router setup
import router from "./routes";

// plugin setup
import { SpinnerPlugin } from "bootstrap-vue";
Vue.use(SpinnerPlugin);
import { FormSelectPlugin } from "bootstrap-vue";
Vue.use(FormSelectPlugin);
Vue.use(DashboardPlugin);
import VueFlatPickr from "vue-flatpickr-component";
import "flatpickr/dist/flatpickr.css";
Vue.use(VueFlatPickr);
Vue.use(VueExcelXlsx);

Vue.use(VueTimeago, {
  name: "Timeago", // Component name, `Timeago` by default
  locale: "en", // Default locale
});

// plugin setup
Vue.use(DashboardPlugin);

// global library setup
Vue.prototype.$Chartist = Chartist;

//Api Stuff
window.api = api;
Vue.prototype.$http = api;

api.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("S2020_AUTHKEY");
    if (token != null && token != "null" && token != "undefined") {
      config.headers.common["Authorization"] = "Bearer " + token;
    } else if (store.state.Auth != null && store.state.Auth != undefined) {
      config.headers.common["Authorization"] = "Bearer " + store.state.Auth;
    }
    if (localStorage.getItem("S2020_USERTYPE") == "PARTNERADMIN") {
      config.data.append(
        "imitateCustomer",
        localStorage.getItem("S2020_imitate_customer")
      );
    } else if (store.state.userProfile.user_type == "PARTNERADMIN") {
      config.data.append("imitateCustomer", store.state.Imitate_Customer_store);
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  (response) => {
    if (response.status === 200 || response.status === 201) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
  (error) => {
    if (
      typeof error.response !== "undefined" &&
      typeof error.response.status !== "undefined"
    ) {
      switch (error.response.status) {
        case 400:
          //Bad Request
          break;
        case 401:
          //Not Authorized
          store.dispatch("authfack/logout");
          break;
        case 403:
          //Forbidden Request
          break;
      }
      return Promise.reject(error.response);
    } else {
      //All other errors that happen preflight. CORS errors. Some 401 Errors.
      if (store.state.Auth != null) {
        //if we have a token and an unknown error, logout if it is expired
        var base64Url = store.state.Auth.split(".")[1];
        var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
        var jsonPayload = decodeURIComponent(
          atob(base64)
            .split("")
            .map(function (c) {
              return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
        );
        var token_expiration = JSON.parse(jsonPayload).exp;
        if (Date.now() - token_expiration * 1000 > 0) {
          store.dispatch("authfack/logout");
        }
      }
      return Promise.reject(error);
    }
  }
);

window.fleetapi = fleetapi;

fleetapi.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("S2020_AUTHKEY");
    if (token != null && token != "null" && token != "undefined") {
      config.headers.common["Authorization"] = "Bearer " + token;
    } else if (store.state.Auth != null && store.state.Auth != "null") {
      config.headers.common["Authorization"] = "Bearer " + store.state.Auth;
    }
    if (localStorage.getItem("S2020_USERTYPE") == "PARTNERADMIN") {
      if (config.method == "post") {
        config.data.append(
          "imitateCustomer",
          localStorage.getItem("S2020_imitate_customer")
        );
      } else {
      }
    } else if (store.state.User_Type == "PARTNERADMIN") {
      if (config.method == "post") {
        config.data.append(
          "imitateCustomer",
          store.state.Imitate_Customer_store
        );
      } else {
      }
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

fleetapi.interceptors.response.use(
  (response) => {
    if (response.status === 200 || response.status === 201) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
  (error) => {
    if (
      typeof error.response !== "undefined" &&
      typeof error.response.status !== "undefined"
    ) {
      switch (error.response.status) {
        case 400:
          //Bad Request
          break;
        case 401:
          //Not Authorized
          if (
            error.response.withCredentials == true ||
            store.state.Auth != null
          ) {
            store.dispatch("authfack/logout");
          }
          break;
        case 403:
          //Forbidden Request
          break;
      }
      return Promise.reject(error.response);
    } else {
      //All other errors that happen preflight. CORS errors. Some 401 Errors.
      if (store.state.Auth != null) {
        //if we have a token and an unknown error, logout if it is expired
        var base64Url = store.state.Auth.split(".")[1];
        var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
        var jsonPayload = decodeURIComponent(
          atob(base64)
            .split("")
            .map(function (c) {
              return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
        );
        var token_expiration = JSON.parse(jsonPayload).exp;
        if (Date.now() - token_expiration * 1000 > 0) {
          store.dispatch("authfack/logout");
        }
      }
      return Promise.reject(error);
    }
  }
);

window.onyxapi = onyxapi;

onyxapi.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("S2020_AUTHKEY");
    if (token != null && token != "null") {
      config.headers.common["Authorization"] = "Bearer " + token;
    } else if (store.state.Auth != null) {
      config.headers.common["Authorization"] = "Bearer " + store.state.Auth;
    }
    if (localStorage.getItem("S2020_USERTYPE") == "PARTNERADMIN") {
      config.data.append(
        "imitateCustomer",
        localStorage.getItem("S2020_imitate_customer")
      );
    } else if (store.state.userProfile.user_type == "PARTNERADMIN") {
      config.data.append("imitateCustomer", store.state.Imitate_Customer_store);
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

onyxapi.interceptors.response.use(
  (response) => {
    if (response.status === 200 || response.status === 201) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
  (error) => {
    if (
      typeof error.response !== "undefined" &&
      typeof error.response.status !== "undefined"
    ) {
      switch (error.response.status) {
        case 400:
          //Bad Request
          break;
        case 401:
          //Not Authorized
          store.dispatch("authfack/logout");
          break;
        case 403:
          //Forbidden Request
          break;
      }
      return Promise.reject(error.response);
    } else {
      //All other errors that happen preflight. CORS errors. Some 401 Errors.
      if (store.state.Auth != null) {
        //if we have a token and an unknown error, logout if it is expired
        var base64Url = store.state.Auth.split(".")[1];
        var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
        var jsonPayload = decodeURIComponent(
          atob(base64)
            .split("")
            .map(function (c) {
              return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
            })
            .join("")
        );
        var token_expiration = JSON.parse(jsonPayload).exp;
        if (Date.now() - token_expiration * 1000 > 0) {
          store.dispatch("authfack/logout");
        }
      }
      return Promise.reject(error);
    }
  }
);

//End API Stuff

/* eslint-disable no-new */
new Vue({
  el: "#app",
  render: (h) => h(App),
  router,
  store,
});
