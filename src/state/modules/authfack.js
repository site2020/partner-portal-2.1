import api from "@/services/api";
import Swal from "sweetalert2";
import router from "../../routes";

const user = JSON.parse(localStorage.getItem("user"));
export const state = user
  ? { status: { loggeduser: true }, user }
  : { status: { loggeduser: false }, user: null };
export const actions = {
  resetpassword({ dispatch, commit }, { email }) {
    const params = new FormData();
    params.append("action", "recoverExternalAccount");
    params.append("recoveryEmail", email);
    api
      .post("/jsonhandler.php", params)
      .then(function (response) {
        if (response != null) {
        }
      })
      .catch(function (response) {
        commit("loginFailure", response);
      });
  },

  // Logs in the user.
  // eslint-disable-next-line no-unused-vars
  login({ dispatch, commit }, { email, password }) {
    commit("loginRequest", { email });
    const params = new FormData();
    params.append("action", "login");
    params.append("username", email);
    params.append("passphrase", password);
    api
      .post("/jsonhandler.php", params)
      .then(function (response) {
        if (response.status === 200) {
          commit("loginSuccess", response);
          router.push({ name: "Dashboard" });
        }
      })
      .catch(function (response) {
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: "md-button md-success",
          },
          buttonsStyling: false,
        });
        swalWithBootstrapButtons.fire({
          title: "Login Error!",
          text: "Invalid Username or Password!",
          icon: "error",
          confirmButtonText: "Continue",
        });
        commit("loginFailure", response);
      });
  },
  // Logout the user
  logout({ commit }) {
    commit("logout");
  },
};

export const mutations = {
  loginRequest(state, user) {
    state.status = { loggingIn: true };
    state.user = user;
  },
  loginSuccess(state, token) {
    state.status = { loggeduser: true };
    localStorage.setItem("S2020_AUTHKEY", token.data.token);
    localStorage.setItem("S2020_USERTYPE", token.data.user_type);
  },
  loginFailure(state) {
    state.status = {};
    state.user = null;
  },
  logout(state) {
    state.status = { loggeduser: false };
    localStorage.removeItem("S2020_AUTHKEY", null);
    localStorage.removeItem("S2020_USERTYPE", null);
    localStorage.removeItem("S2020_imitate_customer", null);
    router.push({ name: "Login" });
  },
};
