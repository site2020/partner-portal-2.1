import onyxapi from "../../services/onyxapi";

export const state = {
  car_counting_enabled: null,
  email: "",
  first_name: "",
  id: null,
  last_name: "",
  name: "",
  phone_number: null,
  timezone: "",
  user_type: "",
  vision_enabled: null,
  daily_onyx_summary_email: "",
  LastLoginDate: "",
  completed_jobs: null,
  all_jobs: null,
  user_avatar: null,
  Lat: null,
  Lng: null,
};

export const mutations = {
  setprofile(state, userprofile) {
    state.car_counting_enabled = userprofile.car_counting_enabled;
    state.email = userprofile.email;
    state.first_name = userprofile.first_name;
    state.id = userprofile.id;
    state.last_name = userprofile.last_name;
    state.name = userprofile.name;
    state.phone_number = userprofile.phone_number;
    state.timezone = userprofile.timezone;
    state.user_type = userprofile.user_type;
    state.vision_enabled = userprofile.vision_enabled;
    state.LastLoginDate = userprofile.LastLoginDate;
    state.ExternalParseBatteryLevel = userprofile.ExternalParseBatteryLevel;
    state.ExternalParseTabletBattery = userprofile.ExternalParseTabletBattery;
    state.ExternalParseUSBAlert = userprofile.ExternalParseUSBAlert;
    state.BaseLeftOnTooLong = userprofile.BaseLeftOnTooLong;
    state.invoice_style = userprofile.invoice_style;
    state.user_avatar = userprofile.user_avatar;
    state.Lat = userprofile.lat;
    state.Lng = userprofile.lng;
  },
  getcompletedjobs(state, getcompletedjobs) {
    state.completed_jobs = getcompletedjobs.Completed_Jobs;
  },
  getalljobs(state, getalljobs) {
    state.all_jobs = getalljobs.All_Jobs;
  },
};

export const actions = {
  setprofile({ commit }) {
    const params = new FormData();
    params.append("action", "getUserProfile");
    fleetapi.post("/getuserprofile", params).then(function (response) {
      if (response != null) {
        commit("setprofile", response.data[0]);
      }
    });
  },
};
