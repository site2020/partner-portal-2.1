import onyxapi from "../../services/onyxapi";

//Customer List for the client that you are currently imitating.

export const state = {
  customerList: [],
};

export const mutations = {
  allcustomers(state, allcustomers) {
    state.customerList = allcustomers;
  },
};

export const actions = {
  allcustomers({ commit }) {
    const params = new FormData();
    params.append("action", "getCustomerListFromDB");
    onyxapi.post("/getcustomerlist", params).then(function (response) {
      commit("allcustomers", response.data);
    });
  },
};
