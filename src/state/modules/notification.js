import onyxapi from "@/services/onyxapi";

export const state = {
  Notifications: [],
  AlertStatus: [],
};

export const mutations = {
  setnotifications(state, notifications) {
    state.Notifications = notifications;
  },
  setalertstatus(state, status) {
    state.AlertStatus = status;
  },
};

export const actions = {
  GetAlertStatus({ commit }) {
    const params = new FormData();
    fleetapi.post("/getalertstatus", params).then(function (response) {
      if (response != null) {
        commit("setalertstatus", response.data[0]);
      }
    });
  },
  setnotifications({ commit }) {
    const params = new FormData();
    fleetapi.post("/getnotifications", params).then(function (response) {
      if (response != null) {
        commit("setnotifications", response.data);
      }
    });
  },
};
