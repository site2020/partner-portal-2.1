import onyxapi from "../../services/onyxapi";

export const state = {
  allEmployees: [],
};

export const mutations = {
  allemployees(state, allemployees) {
    state.allEmployees = allemployees;
  },
};

export const actions = {
  allemployees({ commit }) {
    const params = new FormData();
    params.append("action", "getEmployeeList");
    onyxapi.post("/getemployeelist", params).then(function (response) {
      if (response != null) {
        commit("allemployees", response.data);
      }
    });
  },
};
