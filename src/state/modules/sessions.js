import api from "@/services/api";

export const state = {
  CurrentTimeFrame: "today",
  Sessions: [],
  starton: null,
  endon: null,
  ExclusionZones: [],
  LastExclusionZonesUpdate: null,
  LastSessionsUpdate: null,
  Months: [],
};

export const mutations = {
  Update_Sessions_Timeframe(state, newmonth) {
    state.CurrentTimeFrame = newmonth;
  },
  Update_Sessions(state, newSessions) {
    state.Sessions = newSessions;
    state.LastSessionsUpdate = +new Date();
  },
  Update_Months(state, newMonths) {
    state.Months = newMonths;
    state.LastMonthsUpdate = +new Date();
  },
  Update_Start_On(state, startdate) {
    state.startOn = startdate;
  },
  Update_End_On(state, enddate) {
    state.endOn = enddate;
  },
  Update_Exclusion_Zones(state, data) {
    state.ExclusionZones = data;
    state.LastExclusionZonesUpdate = +new Date();
  },
};

export const actions = {
  GetInvoiceMonths(context) {
    const params = new FormData();
    params.append("action", "getInvoiceMonths");
    api.post("/jsonhandler.php", params).then(function (response) {
      context.commit("Update_Months", response.data);
    });
  },
  GetExclusionZones(context) {
    const params = new FormData();
    params.append("action", "getExclusionZones");
    api.post("/jsonhandler.php", params).then(function (response) {
      context.commit("Update_Exclusion_Zones", response.data);
    });
  },
  getsessions(context, data) {
    const params = new FormData();
    if (data == undefined) {
    } else {
      params.append("startOn", data.startOn);
      params.append("endOn", data.endOn);
    }
    params.append("action", "getSessions");
    params.append("month", state.CurrentTimeFrame);
    api.post("/jsonhandler.php", params).then(function (response) {
      context.commit("Update_Sessions", response.data);
    });
  },
  ChangeSessionTimeFrame(context, newmonth, starton, endon) {
    if (state.CurrentTimeFrame != newmonth) {
      context.commit("Update_Sessions_Timeframe", newmonth);
      context.commit("Update_Start_On", starton);
      context.commit("Update_End_On", endon);
    }
  },
};
