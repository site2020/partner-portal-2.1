import { mapState, mapGetters, mapActions } from "vuex";

export const authFackMethods = mapActions("authfack", [
  "login",
  "registeruser",
  "logout",
  "resetpassword",
]);

export const notificationMethods = mapActions("notification", [
  "setnotifications",
  "GetAlertStatus",
  "setalertstatus",
]);

export const userprofileMethods = mapActions("userProfile", [
  "setprofile",
  "getcompletedjobs",
  "getalljobs",
]);

export const customerListMethods = mapActions("customerList", ["allcustomers"]);

export const employeeListMethods = mapActions("employeeList", ["allemployees"]);

export const sessionsMethods = mapActions("sessions", [
  "getsessions",
  "GetExclusionZones",
  "ChangeSessionsMonth",
  "GetInvoiceMonths",
]);
