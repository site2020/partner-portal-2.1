import axios from "axios";
export default axios.create({
  baseURL: process.env.VUE_APP_API,
  timeout: 120000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json; charset=UTF-8",
  },
});
