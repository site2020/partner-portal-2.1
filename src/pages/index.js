import EditProfileForm from "./Dashboard/Pages/UserProfile/EditProfileForm.vue";
import UserCard from "./Dashboard/Pages/UserProfile/UserCard.vue";
import EditNewsPostForm from "./Dashboard/Pages/NewsPosts/EditNewsPostForm.vue";
import ListOfNewsPosts from "./Dashboard/Pages/NewsPosts/ListOfNewsPosts.vue";
import AddUserContent from "./Dashboard/Pages/EmployeeManagement/Employee-Accounts/Add-User/AddUserContent.vue";
import ManageUserContent from "./Dashboard/Pages/EmployeeManagement/Employee-Accounts/Manage-User/ManageUserContent.vue";
import ViewSessionsContent from "./Dashboard/Pages/Sessions/ViewSessions/ViewSessionsContent.vue";
import ViewInvoicesContent from "./Dashboard/Pages/Sessions/ViewInvoices/ViewInvoicesContent.vue";
import BasestationPassthroughContent from "./Dashboard/Pages/BasestationPassthrough/BasestationPassthroughContent.vue";
import FeedbackContent from "./Dashboard/Pages/StayInTouch/Feedback/FeedbackContent.vue";
import NewsContent from "./Dashboard/Pages/StayInTouch/News/NewsContent";
import OnyxContent from "./Dashboard/Marketing/Onyx/OnyxContent";
import ProtectorContent from "./Dashboard/Marketing/ProtectorCone/ProtectorContent";
import TrackersContent from "./Dashboard/Marketing/Trackers/TrackersContent";
import GateStatusContent from "./Dashboard/Pages/BasestationPassthrough/GateStatus.vue";
import CustomerSuccessReportContent from "./Dashboard/Pages/Site2020Reporting/CustomerSuccessReport/CustomerSuccessReportContent";
import DepSalesManagementContent from "./Dashboard/Pages/Site2020Reporting/DepSalesManagement/DepSalesManagementContent";
import FleetUsageReportContent from "./Dashboard/Pages/Site2020Reporting/FleetUsageReport/FleetUsageReportContent";
import ManageNagContent from "./Dashboard/Pages/Site2020Reporting/ManageNag/ManageNagContent";
import SubOfficesContent from "./Dashboard/Pages/SubOfficeManagement/SubOfficesContent";

export {
  EditProfileForm,
  UserCard,
  ListOfNewsPosts,
  EditNewsPostForm,
  AddUserContent,
  ManageUserContent,
  ViewSessionsContent,
  ViewInvoicesContent,
  BasestationPassthroughContent,
  FeedbackContent,
  NewsContent,
  OnyxContent,
  ProtectorContent,
  TrackersContent,
  GateStatusContent,
  CustomerSuccessReportContent,
  DepSalesManagementContent,
  FleetUsageReportContent,
  ManageNagContent,
  SubOfficesContent,
};
